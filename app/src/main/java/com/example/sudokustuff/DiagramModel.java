package com.example.sudokustuff;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class DiagramModel implements Serializable {
    private ArrayList<DiagramElementModel> elements = new ArrayList<>();
    private boolean solved;

    public DiagramModel(String[] values) {
        for (String value : values) {
            this.elements.add(new DiagramElementModel(value));
        }
        this.solved = false;
    }

    public ArrayList<DiagramElementModel> getElements(){
        return this.elements;
    }

    public DiagramElementModel getElement(int position) {
        return this.elements.get(position);
    }

    public boolean isSolved() {
        return this.solved;
    }

    public void setSolved(boolean value) {
        this.solved = value;
    }

    public boolean isValid() {
        return checkColumns()&&checkRows()&&checkDiagramParts();
    }

    private boolean checkRows() {
        for (int i=0; i<81; i+=9) {
            if (elements.subList(i, i+9).size()!=((HashSet)elements.subList(i, i+9)).size()) return false;
        }
        return true;
    }

    private boolean checkColumns() {
        for (int i=0; i<9; i++) {
            ArrayList<String> column = new ArrayList<>();
            for (int j=i; j<81; j+=9) {
                column.add(elements.get(j).getValue());
            }
            Set<String> s = new HashSet<>(column);
            if (column.size()!=s.size()) return false;
        }
        return true;
    }

    private boolean checkDiagramParts() {
        for (int i=0; i<81; i+=27) {
            ArrayList<String> subset = new ArrayList<>();
            subset.add(elements.get(i).getValue());
            subset.add(elements.get(i+1).getValue());
            subset.add(elements.get(i+2).getValue());
            subset.add(elements.get(i+9).getValue());
            subset.add(elements.get(i+10).getValue());
            subset.add(elements.get(i+11).getValue());
            subset.add(elements.get(i+18).getValue());
            subset.add(elements.get(i+19).getValue());
            subset.add(elements.get(i+20).getValue());
            Set<String> s = new HashSet<>(subset);
            if (subset.size()!=s.size()) return false;
        }
        return true;
    }

}
