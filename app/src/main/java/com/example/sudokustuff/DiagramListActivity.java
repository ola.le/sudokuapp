package com.example.sudokustuff;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class DiagramListActivity extends AppCompatActivity {

    ArrayList<DiagramModel> diagrams = new ArrayList<>();
    String[] defaultDiagram = {"4", "", "", "", "", "1", "9", "", "8",
            "", "", "", "", "9", "6", "2", "", "1",
            "", "1", "", "2", "", "", "", "", "",
            "3", "4", "2", "7", "1", "", "", "", "",
            "9", "7", "", "", "2", "", "", "1", "3",
            "", "", "", "", "5", "3", "4", "2", "7",
            "", "", "", "", "", "5", "", "3", "",
            "1", "", "6", "3", "4", "", "", "", "",
            "7", "", "3", "1", "", "", "", "", "9"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.diagram_list);

        InitDemoData();

        RecyclerView recyclerView = findViewById(R.id.diagram_list_recycler);
        DiagramListAdapter adapter = new DiagramListAdapter(this, diagrams);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 4));
    }


    private void InitDemoData() {
        diagrams = new ArrayList<>();
        for (int i=0; i<15; i++) {
            diagrams.add(new DiagramModel(defaultDiagram));
        }
        diagrams.get(0).setSolved(true);
    }
}
