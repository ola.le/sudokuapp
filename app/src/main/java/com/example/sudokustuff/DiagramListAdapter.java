package com.example.sudokustuff;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.Serializable;
import java.util.ArrayList;

public class DiagramListAdapter extends RecyclerView.Adapter<DiagramListAdapter.ViewHolder> {

    ArrayList<DiagramModel> mDiagrams;
    Context mContext;

    public DiagramListAdapter(Context context, ArrayList<DiagramModel> diagrams) {
        this.mContext = context;
        this.mDiagrams = diagrams;
    }

    @NonNull
    @Override
    public DiagramListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.diagramlist_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull DiagramListAdapter.ViewHolder holder, final int position) {
        if (mDiagrams.get(position).isSolved()) holder.button.setText("OK");
        else holder.button.setText(String.valueOf(position));

        holder.button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, DiagramActivity.class);
                intent.putExtra("diagram", (Serializable) mDiagrams.get(position));
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mDiagrams.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        Button button;
        RelativeLayout relativeLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            button = itemView.findViewById(R.id.single_diagram_item);
            relativeLayout = itemView.findViewById(R.id.diagram_list_relative);
        }
    }
}
