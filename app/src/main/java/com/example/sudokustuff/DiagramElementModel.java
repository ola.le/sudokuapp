package com.example.sudokustuff;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

public class DiagramElementModel implements Serializable {
    private String value;
    private boolean enabled;

    public DiagramElementModel(String value, boolean enabled) {
        this.value = value;
        this.enabled = enabled;
    }

    public DiagramElementModel(String value) {
        this.value = value;
        this.enabled = value.length()==0;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    public boolean isEnabled() {
        return this.enabled;
    }

}
