package com.example.sudokustuff;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class DiagramActivity extends AppCompatActivity {

    private DiagramModel mDiagram;
    private Button btn_selected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDiagram = (DiagramModel) getIntent().getSerializableExtra("diagram");

        initRecyclerView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_list_item:
                OpenDiagramList();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        DiagramAdapter adapter = new DiagramAdapter(this, mDiagram.getElements());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 9));
    }

    public void SelectDiagramItem(View view) {
        if (btn_selected!=null) btn_selected.setSelected(false);
        btn_selected = (Button) view;
        btn_selected.setSelected(true);
        enableSelectionButtons();
    }

    public void OpenDiagramList() {
        Intent intent = new Intent(this, DiagramListActivity.class);
        startActivity(intent);
    }

    public void SelectOptionItem(View view) {
        Button option_to_select = (Button) view;
        btn_selected.setText(option_to_select.getText());
    }

    public void ResetDiagram(View view) {
        ViewGroup v = findViewById(R.id.recycler_view);
        for (int i = 0; i < v.getChildCount(); i++) {
            View vi = v.getChildAt(i);
            View button = vi.findViewById(R.id.button_item);
            if (button instanceof Button) {
                if (button.isEnabled()) ((Button) button).setText("");
            }
        }
        TextView t = findViewById(R.id.validation_text);
        t.setText("");
    }

    public void CheckDiagram(View view) {

        TextView t = findViewById(R.id.validation_text);
        if(mDiagram.isValid()) {
            t.setText("Diagram is valid!");
            mDiagram.setSolved(true);
        } else {
            t.setText("Diagram is not valid!");
        }
    }

    private void enableSelectionButtons() {
        findViewById(R.id.button_one).setEnabled(true);
        findViewById(R.id.button_two).setEnabled(true);
        findViewById(R.id.button_three).setEnabled(true);
        findViewById(R.id.button_four).setEnabled(true);
        findViewById(R.id.button_five).setEnabled(true);
        findViewById(R.id.button_six).setEnabled(true);
        findViewById(R.id.button_seven).setEnabled(true);
        findViewById(R.id.button_eight).setEnabled(true);
        findViewById(R.id.button_nine).setEnabled(true);
        findViewById(R.id.button_null).setEnabled(true);
    }
}
